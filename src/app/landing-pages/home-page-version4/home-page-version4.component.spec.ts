import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomePageVersion4Component } from './home-page-version4.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomePageVersion4Component', () => {
  let component: HomePageVersion4Component;
  let fixture: ComponentFixture<HomePageVersion4Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ HomePageVersion4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageVersion4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
