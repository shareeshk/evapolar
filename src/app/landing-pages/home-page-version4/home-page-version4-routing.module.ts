import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { HomePageVersion4Component } from './home-page-version4.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Home Page Version 4'
        },
		component : HomePageVersion4Component
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageVersion4RoutingModule { }
