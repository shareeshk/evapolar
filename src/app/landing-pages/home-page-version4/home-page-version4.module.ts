import { NgModule } from '@angular/core';
import { HomePageVersion4RoutingModule } from './home-page-version4-routing.module';
import { HomePageVersion4Component } from './home-page-version4.component';
import {SharedModule} from '../common/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {NgFor} from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    HomePageVersion4RoutingModule,
	SharedModule,
	CarouselModule,
	CdkAccordionModule,
	NgFor,
	LazyLoadImageModule
  ],
  exports: [],
    providers: [],
  declarations: [HomePageVersion4Component]
})
export class HomePageVersion4Module { }