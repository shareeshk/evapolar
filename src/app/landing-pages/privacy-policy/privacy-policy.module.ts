import { NgModule } from '@angular/core';
import { privacyPolicyRoutingModule } from './privacy-policy-routing.module';
import { privacyPolicyComponent } from './privacy-policy.component';
import {SharedModule} from '../common/shared.module';
import {NgFor} from '@angular/common';
@NgModule({
  imports: [
    privacyPolicyRoutingModule,
	SharedModule,
	NgFor
  ],
  exports: [],
    providers: [],
  declarations: [privacyPolicyComponent]
})
export class privacyPolicyModule { }