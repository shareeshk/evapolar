import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { privacyPolicyComponent } from './privacy-policy.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Privacy Policy'
        },
		component : privacyPolicyComponent
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class privacyPolicyRoutingModule { }
