import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { privacyPolicyComponent } from './privacy-policy.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('privacyPolicyComponent', () => {
  let component: privacyPolicyComponent;
  let fixture: ComponentFixture<privacyPolicyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ privacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(privacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
