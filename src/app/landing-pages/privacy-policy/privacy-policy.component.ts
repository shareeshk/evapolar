import { Component, OnInit, ViewChild, TemplateRef, Renderer2, Inject  } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, FormBuilder, FormGroup, UntypedFormGroup, FormArray, UntypedFormArray, ValidatorFn, Validators, FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { Location, DOCUMENT,  isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BrowserModule,Title, Meta } from '@angular/platform-browser'
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
  providers: [BsModalService]
})
export class privacyPolicyComponent implements OnInit {
constructor(private fb: UntypedFormBuilder,private modalService: BsModalService,private titleService: Title,private metaTagService: Meta, @Inject(DOCUMENT) private document: Document){}
ngOnInit(){
this.titleService.setTitle("Privacy Policy | Evapoler Eco Cooling Solution");  
this.metaTagService.updateTag({ property: 'og:type', content: 'website'});
this.metaTagService.updateTag({ property: 'og:locale', content: 'en_US'});  
this.metaTagService.updateTag({ property: 'og:site_name', content: 'Evapoler'});
this.metaTagService.updateTag({ property: 'og:title', content: 'Privacy Policy | Evapoler Eco Cooling Solution'});
this.metaTagService.updateTag({ name: 'twitter:card', content: 'summary'});
this.metaTagService.updateTag({ name: 'twitter:site', content: '@Evapoler'});
this.metaTagService.updateTag({ property: 'twitter:title', content: 'Privacy Policy | Evapoler Eco Cooling Solution'});
}
}