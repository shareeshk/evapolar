import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { thankYouComponent } from './thank-you.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('thankYouComponent', () => {
  let component: thankYouComponent;
  let fixture: ComponentFixture<thankYouComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ thankYouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(thankYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
