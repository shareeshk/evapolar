import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { thankYouComponent } from './thank-you.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Thank You'
        },
		component : thankYouComponent
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class thankYouRoutingModule { }
