import { NgModule } from '@angular/core';
import { thankYouRoutingModule } from './thank-you-routing.module';
import { thankYouComponent } from './thank-you.component';
import {SharedModule} from '../common/shared.module';
import {NgFor} from '@angular/common';
@NgModule({
  imports: [
    thankYouRoutingModule,
	SharedModule,
	NgFor
  ],
  exports: [],
    providers: [],
  declarations: [thankYouComponent]
})
export class thankYouModule { }