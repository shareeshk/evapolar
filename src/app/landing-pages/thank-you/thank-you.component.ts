import { Component, OnInit, ViewChild, TemplateRef, Renderer2, Inject  } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, FormBuilder, FormGroup, UntypedFormGroup, FormArray, UntypedFormArray, ValidatorFn, Validators, FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Location, DOCUMENT,  isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BrowserModule,Title, Meta } from '@angular/platform-browser'
@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.scss'],
	providers: [BsModalService]
})
export class thankYouComponent implements OnInit {
  constructor(private fb: UntypedFormBuilder,private modalService: BsModalService,private titleService: Title,private metaTagService: Meta, @Inject(DOCUMENT) private document: Document){
	
		
  }
  
  ngOnInit(){
this.titleService.setTitle("Thank You | Evapoler Eco Cooling Solution");  
this.metaTagService.updateTag({ property: 'og:type', content: 'website'});
this.metaTagService.updateTag({ property: 'og:locale', content: 'en_US'});  
this.metaTagService.updateTag({ property: 'og:site_name', content: 'Evapoler'});
this.metaTagService.updateTag({ property: 'og:title', content: 'Thank You | Evapoler Eco Cooling Solution'});
this.metaTagService.updateTag({ name: 'twitter:card', content: 'summary'});
this.metaTagService.updateTag({ name: 'twitter:site', content: '@Evapoler'});
this.metaTagService.updateTag({ property: 'twitter:title', content: 'Thank You | Evapoler Eco Cooling Solution'});
}
}