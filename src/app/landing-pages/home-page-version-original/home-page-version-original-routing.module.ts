import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { HomePageVersionOriginalComponent } from './home-page-version-original.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Home Page Version Original'
        },
		component : HomePageVersionOriginalComponent
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageVersionOriginalRoutingModule { }
