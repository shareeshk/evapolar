import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomePageVersionOriginalComponent } from './home-page-version-original.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomePageVersionOriginalComponent', () => {
  let component: HomePageVersionOriginalComponent;
  let fixture: ComponentFixture<HomePageVersionOriginalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ HomePageVersionOriginalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageVersionOriginalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
