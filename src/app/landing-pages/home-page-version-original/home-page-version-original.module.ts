import { NgModule } from '@angular/core';
import { HomePageVersionOriginalRoutingModule } from './home-page-version-original-routing.module';
import { HomePageVersionOriginalComponent } from './home-page-version-original.component';
import {SharedModule} from '../common/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {NgFor} from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    HomePageVersionOriginalRoutingModule,
	SharedModule,
	CarouselModule,
	CdkAccordionModule,
	NgFor,
	LazyLoadImageModule
  ],
  exports: [],
    providers: [],
  declarations: [HomePageVersionOriginalComponent]
})
export class HomePageVersionOriginalModule { }