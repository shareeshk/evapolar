import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomePageVersion5Component } from './home-page-version5.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomePageVersion5Component', () => {
  let component: HomePageVersion5Component;
  let fixture: ComponentFixture<HomePageVersion5Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ HomePageVersion5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageVersion5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
