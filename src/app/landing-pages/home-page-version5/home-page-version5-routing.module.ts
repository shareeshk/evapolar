import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { HomePageVersion5Component } from './home-page-version5.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Home Page Version 5'
        },
		component : HomePageVersion5Component
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageVersion5RoutingModule { }
