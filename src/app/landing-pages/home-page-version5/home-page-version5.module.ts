import { NgModule } from '@angular/core';
import { HomePageVersion5RoutingModule } from './home-page-version5-routing.module';
import { HomePageVersion5Component } from './home-page-version5.component';
import {SharedModule} from '../common/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {NgFor} from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    HomePageVersion5RoutingModule,
	SharedModule,
	CarouselModule,
	CdkAccordionModule,
	NgFor,
	LazyLoadImageModule
  ],
  exports: [],
    providers: [],
  declarations: [HomePageVersion5Component]
})
export class HomePageVersion5Module { }