import { Component, OnInit, ViewChild, TemplateRef, Renderer2, Inject,PLATFORM_ID,HostListener  } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, FormBuilder, FormGroup, UntypedFormGroup, FormArray, UntypedFormArray, ValidatorFn, Validators, FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Location, DOCUMENT,  isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BrowserModule,Title, Meta } from '@angular/platform-browser'
import {ConnectionService} from '../../services/connection.service'
import {ActivatedRoute, Router} from '@angular/router';

export function MobileValidations(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const check_mobile: RegExp = /^[5|6|7|8|9][0-9]*$/;
    const check_digit_0: RegExp = /^[0]{10}$/;
    const check_digit_1: RegExp = /^[1]{10}$/;
    const check_digit_2: RegExp = /^[2]{10}$/;
    const check_digit_3: RegExp = /^[3]{10}$/;
    const check_digit_4: RegExp = /^[4]{10}$/;
    const check_digit_5: RegExp = /^[5]{10}$/;
    const check_digit_6: RegExp = /^[6]{10}$/;
    const check_digit_7: RegExp = /^[7]{10}$/;
    const check_digit_8: RegExp = /^[8]{10}$/;
    const check_digit_9: RegExp = /^[9]{10}$/;

    let error: any = {};

    if (check_digit_0.test(control.value)
      || check_digit_1.test(control.value)
      || check_digit_2.test(control.value)
      || check_digit_3.test(control.value)
      || check_digit_4.test(control.value)
      || check_digit_5.test(control.value)
      || check_digit_6.test(control.value)
      || check_digit_7.test(control.value)
      || check_digit_8.test(control.value)
      || check_digit_9.test(control.value)
    ) {
      error['CHECK_DIGIT_FAILED'] = true;
    }
    else if (!check_mobile.test(control.value)) {
      error['CHECK_MOBILE_FAILED'] = true;
    }
    /*if(!check_special.test(control.value)) {
            error[ 'CHECK_SPECIAL_FAILED' ] = true;
        }
    if(!check_1upperlower.test(control.value)) {
            error[ 'CHECK_1_UPPER_LOWER_FAILED' ] = true;
        }*/

    return Object.keys(error).length > 0 ? error : null;
  };
}

@Component({
  selector: 'app-home-page-version5',
  templateUrl: './home-page-version5.component.html',
  styleUrls: ['./home-page-version5.component.scss'],
	providers: [BsModalService]
})

export class HomePageVersion5Component implements OnInit {
 showTooltip(tooltip: any): void {
    tooltip.show();
  }

  hideTooltip(tooltip: any): void {
    tooltip.hide();
  }
is_reviewcallbackModal_open="no";
@ViewChild('reviewcallbackModal') reviewcallbackModalcontent: TemplateRef<any>;
reviewcallbackModalRef: BsModalRef;

is_casestudiescallbackModal_open="no";
@ViewChild('casestudiescallbackModal') casestudiescallbackModalcontent: TemplateRef<any>;
casestudiescallbackModalRef: BsModalRef;

is_requestcallbackModal_open="no";
@ViewChild('requestcallbackModal') requestcallbackModalcontent: TemplateRef<any>;
requestcallbackModalRef: BsModalRef;

is_expertConsultationModal_open="no";
@ViewChild('expertConsultationModal') expertConsultationModalcontent: TemplateRef<any>;
expertConsultationModalRef: BsModalRef;

is_costsavingscalcModal_open="no";
@ViewChild('costsavingscalcModal') costsavingscalcModalcontent: TemplateRef<any>;
costsavingscalcModalRef: BsModalRef;

is_onPageLoadImagecallbackModal_open="no";
@ViewChild('onPageLoadImagecallbackModal')onPageLoadImagecallbackModalcontent: TemplateRef<any>;
onPageLoadImagecallbackModalRef: BsModalRef;

@ViewChild('onScrollISCScallbackModal')onScrollISCScallbackModalcontent: TemplateRef<any>;
onScrollISCScallbackModalRef: BsModalRef;

item5CQ=[{"key":"1. Is Evaporative Cooling system effective in an Industrial Space?","value":"During peak summer season when the temperatures inside the factory goes beyond 45° C, Evaporative natural cooling systems can give a drop of 15 to 18° C."},{"key":"2. How do we ensure there is no humidity inside the factory?","value":"Air is cooled through the evaporation principle of water. The latent heat of air is absorbed by water sprinkled on honeycomb pads and the cool air is delivered inside the space."},{"key":"3. Can air travel to long distances without ducts?","value":"They consume 90% less electricity as compared to an air conditioner. To cool an area of 1000 sqft. it consumes only one 1– 1.5 units/hr."},{"key":"4. Do you have an improved cooling machine which is better than our existing cooling system?","value":"Evaporative cooling machines make the dry and hot air moist and cool which makes it comfortable for the workers/occupants in the workspace. It removes excessive dryness from air. But to ensure that moisture is not trapped inside the building, 80% of the air must be exhausted through natural doors and windows or mechanical ventilation fans. This also ensures 100% fresh, clean and cool air being circulated all the time thus reducing the risk of cross contamination."},{"key":"5. Can your cooling system increase Productivity of my workers?","value":"Yes, Evapoler also offers next generation indirect evaporative cooling system which does not add moisture to the air while cooling it. Our Innovative SenHex heat exchanger brings down the temperature of air without making it moist.<br>For more information, watch our video on <a href='https://youtu.be/GNApBvHEIcY' target='_blank'>Indirect Evaporative Cooling</a>."}];
items=[{"key":"1. How much temperature drop can be achieved through an Evaporative cooling system?","value":"During peak summer season when the temperatures inside the factory goes beyond 45° C, Evaporative natural cooling systems can give a drop of 15 to 18° C."},{"key":"2. How is the air cooled through an evaporative cooling machine?","value":"Air is cooled through the evaporation principle of water. The latent heat of air is absorbed by water sprinkled on honeycomb pads and the cool air is delivered inside the space."},{"key":"3. What is the electricity consumption of an evaporative cooling system ?","value":"They consume 90% less electricity as compared to an air conditioner. To cool an area of 1000 sqft. it consumes only one 1– 1.5 units/hr."},{"key":"4. Does it create humidity/increase moisture inside the factory?","value":"Evaporative cooling machines make the dry and hot air moist and cool which makes it comfortable for the workers/occupants in the workspace. It removes excessive dryness from air. But to ensure that moisture is not trapped inside the building, 80% of the air must be exhausted through natural doors and windows or mechanical ventilation fans. This also ensures 100% fresh, clean and cool air being circulated all the time thus reducing the risk of cross contamination."},{"key":"5. Is there an option of dry cooling also?","value":"Yes, Evapoler also offers next generation indirect evaporative cooling system which does not add moisture to the air while cooling it. Our Innovative SenHex heat exchanger brings down the temperature of air without making it moist.<br>For more information, watch our video on <a href='https://youtu.be/GNApBvHEIcY' target='_blank'>Indirect Evaporative Cooling</a>."}];
  expandedIndex = 0;
  title = 'Home Evapoler';
  enquiryForm: UntypedFormGroup;
  requestcallbackForm: UntypedFormGroup;
  ISCSPopupcallbackForm: UntypedFormGroup;
  expertConsultationForm: UntypedFormGroup;
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
	autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: true,
	margin: 20,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }
  installationOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
	margin: 20,
	autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }
  clientOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
	margin: 20,
	autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 3
      },
      400: {
        items: 5
      },
      740: {
        items: 3
      },
      940: {
        items: 8
      }
    },
    nav: false
  }
  caseStudiesOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
	margin: 20,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }
  bannerOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
	autoplay:true,
	autoplayTimeout:3000,
	autoplaySpeed:2000,
	autoplayHoverPause:false,
	margin: 20,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  DirectEvaporativeCooling = '';
  IndirectEvaporativeCooling = '';
  StageEvaporativeCoolingSystem = './assets/images/home/products/StageEvaporativeCoolingSystem.jpg';
  HybridAirConditioning = '';
  IndustrialDuctablecooler = '';
  IndustrialPortableCooler = '';
  energy = '';
  warranty = '';
  lowmaintenance = '';
  prompt = '';
  emission = '';
  custom_solutions = '';
  fresh_clean = '';
  reliability = '';
  areacooled = '';
  airsupplied = '';
  installations = '';
  happy_clients = '';
  years_of_expertise = '';
  Manufacturing = '';
  Factories = '';
  Warehouses = '';
  CommercialComplexes = '';
  ElectricPanelrooms = '';
  CommercialKitchens = '';
  Workshops = '';
  AutomobileIndustry = '';
  Printingandpackagingindustry = '';
  FoodProcessingIndustry = '';
  HealthCareIndustry = '';
  Textilegarmentindustry = '';
  isocertificate = '';
  Certificate1 = '';
  msme = '';
  Award2 = '';
  JayBharatMaruti1 = '';
  MahindraTractors = '';
  MascotPlastics = '';
  RMChemicalDhule2 = '';
  VeeGee3 = '';
  YutikaNaturals = '';
  GlobeCapacitors = '';
  Fackelmann = '';
  RMChemicalDhule1 = '';
  delhi = '';
  
  Submit_book_a_freesite="Submit";
  Submit_get_a_callback="Submit";
  Submit_free_expert="Submit";
  Submit_scrollpopupISCS="Submit";
  section:string="";
  constructor(private fb: UntypedFormBuilder,private modalService: BsModalService,private titleService: Title,private metaTagService: Meta, @Inject(DOCUMENT) private document: Document, @Inject(PLATFORM_ID) private platformId: Object,private connService:ConnectionService,private router: Router,private route : ActivatedRoute,@Inject(DOCUMENT) private doc: Document){
	this.enquiryForm = this.fb.group(
		  {
			name: ['', [Validators.required]],
			mobile_number: ['', [Validators.required, MobileValidations(), Validators.minLength(10), Validators.maxLength(10)]],
			emailId: ['', [Validators.required, Validators.pattern("^(?!test).*$"), Validators.pattern("^(?![a-z0-9._%+-]+@[a-zA-Z0-9._%+-]*test[a-zA-Z0-9._%+-]*).*$"), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+[.][a-z]{2,4}$")]],
			company_name: ['', [Validators.required]],
			application: ['', [Validators.required]],
			message: ['']
		  }
		);
		
		this.requestcallbackForm = this.fb.group(
		  {
			name: ['', [Validators.required]],
			mobile_number: ['', [Validators.required, MobileValidations(), Validators.minLength(10), Validators.maxLength(10)]],
			emailId: ['', [Validators.required, Validators.pattern("^(?!test).*$"), Validators.pattern("^(?![a-z0-9._%+-]+@[a-zA-Z0-9._%+-]*test[a-zA-Z0-9._%+-]*).*$"), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+[.][a-z]{2,4}$")]],
			company_name: ['', [Validators.required]],
			application:['', [Validators.required]],
			city:['', [Validators.required]],
			message: ['']
		  }
		);
		this.expertConsultationForm = this.fb.group(
		  {
			name: ['', [Validators.required]],
			mobile_number: ['', [Validators.required, MobileValidations(), Validators.minLength(10), Validators.maxLength(10)]],
			emailId: ['', [Validators.required, Validators.pattern("^(?!test).*$"), Validators.pattern("^(?![a-z0-9._%+-]+@[a-zA-Z0-9._%+-]*test[a-zA-Z0-9._%+-]*).*$"), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+[.][a-z]{2,4}$")]],
			company_name: ['', [Validators.required]],
			application:['', [Validators.required]],
			city:['', [Validators.required]],
			message: ['']
		  }
		);
		this.ISCSPopupcallbackForm = this.fb.group(
		  {
			name: ['', [Validators.required]],
			mobile_number: ['', [Validators.required, MobileValidations(), Validators.minLength(10), Validators.maxLength(10)]],
			emailId: ['', [Validators.required, Validators.pattern("^(?!test).*$"), Validators.pattern("^(?![a-z0-9._%+-]+@[a-zA-Z0-9._%+-]*test[a-zA-Z0-9._%+-]*).*$"), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+[.][a-z]{2,4}$")]],
			city:['', [Validators.required]]
		  }
		);
		
  }
  


  ngOnInit(){
  if (isPlatformBrowser(this.platformId)) {
		  setTimeout(()=>{
			 this.onScrollISCSCallbackFun();
		}, 30000);
	}

  /*if (isPlatformBrowser(this.platformId)) {
	  setTimeout(()=>{
		 this.onPageLoadImageCallbackFun();
	}, 500);
}*/


/*this.route.params.subscribe(params => {
			
			   this.section=params['section'];
			   //alert(this.section);
			   if(this.section!==undefined){
					this.evapolarScrollFun(this.section,-66);
			   }
			   
			});*/
			
			
this.titleService.setTitle("Industrial Air Cooling Systems | Evapoler Eco Cooling Solution");  
this.metaTagService.updateTag({ name: 'description', content: 'Direct & Indirect Evaporative Cooling Systems for Large Industrial and Commercial Spaces with Energy-Saving & Low-Maintenance Features in India. Get a Quote.' });
this.metaTagService.updateTag({ property: 'og:type', content: 'website'});
this.metaTagService.updateTag({ property: 'og:locale', content: 'en_US'});  
this.metaTagService.updateTag({ property: 'og:site_name', content: 'Evapoler'});
this.metaTagService.updateTag({ property: 'og:title', content: 'Industrial Air Cooling Systems | Evapoler Eco Cooling Solution'});
this.metaTagService.updateTag({ property: 'og:description', content: 'Direct & Indirect Evaporative Cooling Systems for Large Industrial and Commercial Spaces with Energy-Saving & Low-Maintenance Features in India. Get a Quote.'});
this.metaTagService.updateTag({ property: 'og:image', content: 'assets/images/logo/logo-evapoler-new-1.webp'});
this.metaTagService.updateTag({ property: 'og:image:width', content: '238'});
this.metaTagService.updateTag({ property: 'og:image:height', content: '66'});
this.metaTagService.updateTag({ property: 'og:url', content: this.document.URL});
this.metaTagService.updateTag({ property: 'article:publisher', content: 'https://www.facebook.com/EvapolerEcoCoolingSolutions'});
this.metaTagService.updateTag({ name: 'twitter:card', content: 'summary'});
this.metaTagService.updateTag({ name: 'twitter:site', content: '@Evapoler'});
this.metaTagService.updateTag({ property: 'twitter:title', content: 'Industrial Air Cooling Systems | Evapoler Eco Cooling Solution'});
this.metaTagService.updateTag({ property: 'twitter:description', content: 'Direct & Indirect Evaporative Cooling Systems for Large Industrial and Commercial Spaces with Energy-Saving & Low-Maintenance Features in India. Get a Quote.'});
this.metaTagService.updateTag({ name: 'twitter:image', content: 'assets/images/logo/logo-evapoler-new-1.webp'});
}


submitBookingFun(type_of_form:any){
	let desktop_mobile="";
	if (document.body.offsetWidth < 768) {
		desktop_mobile="mobile";
	}
	else{
		desktop_mobile="desktop";
	}
    let bodyParams:any;
    if(type_of_form=="book_a_freesite"){
		bodyParams={
			"name":this.enquiryForm?.get("name")?.value,
			"mobile_number":this.enquiryForm?.get("mobile_number")?.value,
			"emailId":this.enquiryForm?.get("emailId")?.value,
			"company_name":this.enquiryForm?.get("company_name")?.value,
			"application":this.enquiryForm?.get("application")?.value,
			"message":this.enquiryForm?.get("message")?.value,
			"type_of_form":type_of_form,
			"desktop_mobile":desktop_mobile,
			"page_name":"industrialcoolerstelanganakarnataka"
		}
		this.Submit_book_a_freesite="Processing  <i class='fa fa-spin fa-refresh'></i>";
	}
	if(type_of_form=="get_a_callback"){
		bodyParams={
			"name":this.requestcallbackForm?.get("name")?.value,
			"mobile_number":this.requestcallbackForm?.get("mobile_number")?.value,
			"emailId":this.requestcallbackForm?.get("emailId")?.value,
			"company_name":this.requestcallbackForm?.get("company_name")?.value,
			"application":this.requestcallbackForm?.get("application")?.value,
			"city":this.requestcallbackForm?.get("city")?.value,
			"message":this.requestcallbackForm?.get("message")?.value,
			"type_of_form":type_of_form,
			"desktop_mobile":desktop_mobile,
			"page_name":"industrialcoolerstelanganakarnataka"
		}
		this.Submit_get_a_callback="Processing  <i class='fa fa-spin fa-refresh'></i>";
	}
	if(type_of_form=="free_expert"){
		bodyParams={
			"name":this.expertConsultationForm?.get("name")?.value,
			"mobile_number":this.expertConsultationForm?.get("mobile_number")?.value,
			"emailId":this.expertConsultationForm?.get("emailId")?.value,
			"company_name":this.expertConsultationForm?.get("company_name")?.value,
			"application":this.expertConsultationForm?.get("application")?.value,
			"city":this.expertConsultationForm?.get("city")?.value,
			"message":this.expertConsultationForm?.get("message")?.value,
			"type_of_form":type_of_form,
			"desktop_mobile":desktop_mobile,
			"page_name":"industrialcoolerstelanganakarnataka"
		}
		this.Submit_free_expert="Processing  <i class='fa fa-spin fa-refresh'></i>";
	}
	if(type_of_form=="scrollpopupISCS"){
		bodyParams={
			"name":this.ISCSPopupcallbackForm?.get("name")?.value,
			"mobile_number":this.ISCSPopupcallbackForm?.get("mobile_number")?.value,
			"emailId":this.ISCSPopupcallbackForm?.get("emailId")?.value,
			"city":this.ISCSPopupcallbackForm?.get("city")?.value,
			"type_of_form":type_of_form,
			"desktop_mobile":desktop_mobile,
			"page_name":"industrialcoolerstelanganakarnataka"
		}
		this.Submit_scrollpopupISCS="Processing  <i class='fa fa-spin fa-refresh'></i>";
	}
	this.connService.dumpConsultation(bodyParams).subscribe(
		(data:any) => {
		    this.Submit_book_a_freesite="Submit";
			this.Submit_get_a_callback="Submit";
			this.Submit_free_expert="Submit";
			this.Submit_scrollpopupISCS="Submit";
			if(data["status"]=="success" || data["status"]=="exists"){
				if(type_of_form=="get_a_callback"){
					this.requestcallbackModalRef.hide();
				}
				if(type_of_form=="free_expert"){
					this.expertConsultationModalRef.hide();
				}
				if(type_of_form=="scrollpopupISCS"){
					this.onScrollISCScallbackModalRef.hide();
				}
				this.router.navigate(['/thank-you']);
			}
		},
		error => {
			this.Submit_book_a_freesite="Submit";
			this.Submit_get_a_callback="Submit";
			this.Submit_free_expert="Submit";
			this.Submit_scrollpopupISCS="Submit";
			alert("Oops! some issue");
			return false;
		},
		()=>{
			this.Submit_book_a_freesite="Submit";
			this.Submit_get_a_callback="Submit";
			this.Submit_free_expert="Submit";
			this.Submit_scrollpopupISCS="Submit";
		}
	)
  }
  
  
reviewsCallbackFun(){
this.reviewcallbackModalRef = this.modalService.show(this.reviewcallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
this.is_reviewcallbackModal_open="yes";
}
reviewModalRefHideFun(){
	this.reviewcallbackModalRef.hide();
	this.is_reviewcallbackModal_open="no";
}
caseStudiesCallbackFun(){
this.casestudiescallbackModalRef = this.modalService.show(this.casestudiescallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
this.is_casestudiescallbackModal_open="yes";
}
casestudiesModalRefHideFun(){
this.casestudiescallbackModalRef.hide();
this.is_casestudiescallbackModal_open="no";
}
	
	requestCallbackFun(){
		this.requestcallbackForm.reset();
		this.requestcallbackModalRef = this.modalService.show(this.requestcallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
		this.is_requestcallbackModal_open="yes";
	}
	requestcallbackModalRefHideFun(){
		this.requestcallbackModalRef.hide();
		this.is_requestcallbackModal_open="no";
	}
	expertConsultationFun(){
		this.expertConsultationForm.reset();
		this.expertConsultationModalRef = this.modalService.show(this.expertConsultationModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
		this.is_expertConsultationModal_open="yes";
	}
	expertConsultationModalRefHideFun(){
		this.expertConsultationModalRef.hide();
		this.is_expertConsultationModal_open="no";
	}
	
	costsSavingsCalculatorFun(){
		this.area="";
		this.applicationforcalculation="";
		this.Electricity_Consumption_with_Evapoler="";
		this.Electricity_Consumption_with_Evapoler_unit_label="";
		this.Electricity_Consumption_with_AC="";
		this.Electricity_Consumption_with_AC_unit_label="";
		this.Annual_Savings_with_Evapoler="";
		this.costsavingscalcModalRef = this.modalService.show(this.costsavingscalcModalcontent, { class: 'modal-lg modal-dialog-centered', ignoreBackdropClick: true });
		this.is_costsavingscalcModal_open="yes";
	}
	
	costsavingscalcModalRefHideFun(){
		this.costsavingscalcModalRef.hide();
		this.is_costsavingscalcModal_open="no";
	}
	onPageLoadImageCallbackFun(){
this.onPageLoadImagecallbackModalRef = this.modalService.show(this.onPageLoadImagecallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
this.is_onPageLoadImagecallbackModal_open="yes";
}
onPageLoadImageModalRefHideFun(){
this.onPageLoadImagecallbackModalRef.hide();
this.is_onPageLoadImagecallbackModal_open="no";
}
enquiry_now_hidden="no";
onScrollISCSCallbackFun(){

const input = document.getElementById(
  'enquiry_now_hidden',
) as HTMLInputElement | null;

if (input != null) {
  this.enquiry_now_hidden=input.value
}

if(this.is_reviewcallbackModal_open=="no" && this.is_casestudiescallbackModal_open=="no" && this.is_requestcallbackModal_open=="no" && this.is_expertConsultationModal_open=="no" && this.is_costsavingscalcModal_open=="no" && this.is_onPageLoadImagecallbackModal_open=="no" && this.enquiry_now_hidden=="no" && this.enquiryForm?.get("name")?.value=="" && this.enquiryForm?.get("mobile_number")?.value=="" && this.enquiryForm?.get("emailId")?.value=="" && this.enquiryForm?.get("company_name")?.value=="" && this.enquiryForm?.get("message")?.value=="" && this.enquiryForm?.get("application")?.value==""){
	
	this.onScrollISCScallbackModalRef = this.modalService.show(this.onScrollISCScallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
}

}
onScrollISCSModalRefHideFun(){
this.onScrollISCScallbackModalRef.hide();
}
	area="";
	applicationforcalculation="";
	Electricity_Consumption_with_Evapoler="";
	Electricity_Consumption_with_Evapoler_unit_label="";
	Electricity_Consumption_with_AC="";
	Electricity_Consumption_with_AC_unit_label="";
	Annual_Savings_with_Evapoler="";
	areaFun(){
	    if(this.area==""){
			this.Electricity_Consumption_with_Evapoler="";
			this.Electricity_Consumption_with_Evapoler_unit_label="";
			
			this.Electricity_Consumption_with_AC="";
			this.Electricity_Consumption_with_AC_unit_label="";
			
			this.Annual_Savings_with_Evapoler="";
		}
		if(this.area=="1000"){
			this.Electricity_Consumption_with_Evapoler="1";
			this.Electricity_Consumption_with_Evapoler_unit_label="Unit";
			
			this.Electricity_Consumption_with_AC="10";
			this.Electricity_Consumption_with_AC_unit_label="Units";
			
			this.Annual_Savings_with_Evapoler=(9*8*10*25*9).toString();
			this.Annual_Savings_with_Evapoler=new Intl.NumberFormat().format(Number(this.Annual_Savings_with_Evapoler))
		}
		if(this.area=="2000"){
			this.Electricity_Consumption_with_Evapoler="2";
			this.Electricity_Consumption_with_Evapoler_unit_label="Units";
			
			this.Electricity_Consumption_with_AC="20";
			this.Electricity_Consumption_with_AC_unit_label="Units";
			
			this.Annual_Savings_with_Evapoler=(18*8*10*25*9).toString();
			this.Annual_Savings_with_Evapoler=new Intl.NumberFormat().format(Number(this.Annual_Savings_with_Evapoler))
		}
		if(this.area=="3000"){
			this.Electricity_Consumption_with_Evapoler="3";
			this.Electricity_Consumption_with_Evapoler_unit_label="Units";
			
			this.Electricity_Consumption_with_AC="30";
			this.Electricity_Consumption_with_AC_unit_label="Units";
			
			this.Annual_Savings_with_Evapoler=(27*8*10*25*9).toString();
			this.Annual_Savings_with_Evapoler=new Intl.NumberFormat().format(Number(this.Annual_Savings_with_Evapoler))
		}
		if(this.area=="4000"){
			this.Electricity_Consumption_with_Evapoler="4";
			this.Electricity_Consumption_with_Evapoler_unit_label="Units";
			
			this.Electricity_Consumption_with_AC="40";
			this.Electricity_Consumption_with_AC_unit_label="Units";
			
			this.Annual_Savings_with_Evapoler=(36*8*10*25*9).toString();
			this.Annual_Savings_with_Evapoler=new Intl.NumberFormat().format(Number(this.Annual_Savings_with_Evapoler))
		}
		if(this.area=="5000"){
			this.Electricity_Consumption_with_Evapoler="5";
			this.Electricity_Consumption_with_Evapoler_unit_label="Units";
			
			this.Electricity_Consumption_with_AC="50";
			this.Electricity_Consumption_with_AC_unit_label="Units";
			
			this.Annual_Savings_with_Evapoler=(45*8*10*25*9).toString();
			this.Annual_Savings_with_Evapoler=new Intl.NumberFormat().format(Number(this.Annual_Savings_with_Evapoler))
		}
	}
}