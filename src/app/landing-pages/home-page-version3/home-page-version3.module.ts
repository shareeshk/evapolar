import { NgModule } from '@angular/core';
import { HomePageVersion3RoutingModule } from './home-page-version3-routing.module';
import { HomePageVersion3Component } from './home-page-version3.component';
import {SharedModule} from '../common/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {NgFor} from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    HomePageVersion3RoutingModule,
	SharedModule,
	CarouselModule,
	CdkAccordionModule,
	NgFor,
	LazyLoadImageModule
  ],
  exports: [],
    providers: [],
  declarations: [HomePageVersion3Component]
})
export class HomePageVersion3Module { }