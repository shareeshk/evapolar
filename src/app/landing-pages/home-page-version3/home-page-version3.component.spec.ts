import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomePageVersion3Component } from './home-page-version3.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomePageVersion3Component', () => {
  let component: HomePageVersion3Component;
  let fixture: ComponentFixture<HomePageVersion3Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
	  imports: [
        HttpClientTestingModule 
      ],
      declarations: [ HomePageVersion3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageVersion3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
