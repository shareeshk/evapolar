import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { HomePageVersion3Component } from './home-page-version3.component';
const routes: Routes = [
	{
		path: '',
		data: {
            title: 'Home Page Version 3'
        },
		component : HomePageVersion3Component
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageVersion3RoutingModule { }
