import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { HttpClient } from '@angular/common/http';
import { MatchHeightDirective } from './match-height.directive';
@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
		MatInputModule,
		MatSelectModule,
		MatFormFieldModule,
		MatButtonModule,
    MatTooltipModule,
	MatButtonToggleModule,
    ],
    declarations: [MatchHeightDirective],
    exports: [
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
		MatInputModule,
		MatSelectModule,
		MatFormFieldModule,
		MatButtonModule,
    MatTooltipModule,
	MatButtonToggleModule,
	MatchHeightDirective
		],
		providers: []
		
})
export class SharedModule {
    static forRoot(): ModuleWithProviders<SharedModule> {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}