import {
    Directive, ElementRef, AfterViewChecked, 
    Input, HostListener,PLATFORM_ID,Inject
} from '@angular/core';
import { Location, DOCUMENT,  isPlatformBrowser, isPlatformServer } from '@angular/common';
@Directive({
    selector: '[myMatchHeight]'
})
export class MatchHeightDirective implements AfterViewChecked {

    // class name to match height
    @Input()
    myMatchHeight: any;

    constructor(private el: ElementRef,@Inject(PLATFORM_ID) private platformId: Object) {
    }
	matchHeight_i:number=0;
    ngAfterViewChecked() {
        // call our matchHeight function here later
        if(this.matchHeight_i==0){
			setTimeout(()=>this.matchHeight(this.el.nativeElement, this.myMatchHeight),200)
			this.matchHeight_i++;
		}
    }
	matchHeight_j:number=0;
    @HostListener('window:resize') 
    onResize() {
        // call our matchHeight function here later
        if(this.matchHeight_j==0){
			setTimeout(()=>this.matchHeight(this.el.nativeElement, this.myMatchHeight),200)
			this.matchHeight_j++;
		}
    }
    matchHeight_k:number=0;
    @HostListener('window:scroll', ['$event'])
    onWindowScroll() {
        // call our matchHeight function here later
        if(this.matchHeight_k==0){
			setTimeout(()=>this.matchHeight(this.el.nativeElement, this.myMatchHeight),200)
			this.matchHeight_k++;
		}
    }

    matchHeight(parent: HTMLElement, className: string) {
        // match height logic here

        if (!parent) return;
        const children = parent.getElementsByClassName(className);

        if (!children) return;

        // reset all children height
        Array.from(children).forEach((x: Element) => {
			if (x instanceof HTMLElement) {
				x.style.height = 'initial';
			}
        })
		if (isPlatformBrowser(this.platformId)) {
        // gather all height
        const itemHeights = Array.from(children)
            .map(x => x.getBoundingClientRect().height);

        // find max height
        const maxHeight = itemHeights.reduce((prev, curr) => {
            return curr > prev ? curr : prev;
        }, 0);

        // apply max height
        //Array.from(children)
            //.forEach((x: HTMLElement) => x.style.height = `${maxHeight}px`);
			
		Array.from(children).forEach((x: Element) => {
			if (x instanceof HTMLElement) {
				x.style.height = `${maxHeight}px`
			}
        })
		}
    }
}