import { Component, OnInit, ViewChild, TemplateRef, Renderer2, Inject, AfterViewChecked } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, FormBuilder, FormGroup, UntypedFormGroup, FormArray, UntypedFormArray, ValidatorFn, Validators, FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { DOCUMENT,Location } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {ConnectionService} from './services/connection.service'
import {ActivatedRoute, Router} from '@angular/router';
export function MobileValidations(): ValidatorFn {
return (control: AbstractControl): { [key: string]: any } | null => {
const check_mobile: RegExp = /^[5|6|7|8|9][0-9]*$/;
const check_digit_0: RegExp = /^[0]{10}$/;
const check_digit_1: RegExp = /^[1]{10}$/;
const check_digit_2: RegExp = /^[2]{10}$/;
const check_digit_3: RegExp = /^[3]{10}$/;
const check_digit_4: RegExp = /^[4]{10}$/;
const check_digit_5: RegExp = /^[5]{10}$/;
const check_digit_6: RegExp = /^[6]{10}$/;
const check_digit_7: RegExp = /^[7]{10}$/;
const check_digit_8: RegExp = /^[8]{10}$/;
const check_digit_9: RegExp = /^[9]{10}$/;
let error: any = {};
if (check_digit_0.test(control.value)
|| check_digit_1.test(control.value)
|| check_digit_2.test(control.value)
|| check_digit_3.test(control.value)
|| check_digit_4.test(control.value)
|| check_digit_5.test(control.value)
|| check_digit_6.test(control.value)
|| check_digit_7.test(control.value)
|| check_digit_8.test(control.value)
|| check_digit_9.test(control.value)
) {
error['CHECK_DIGIT_FAILED'] = true;
}
else if (!check_mobile.test(control.value)) {
error['CHECK_MOBILE_FAILED'] = true;
}
return Object.keys(error).length > 0 ? error : null;
};
}
@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.scss'],
providers: [BsModalService]
})
export class AppComponent  {
@ViewChild('requestcallbackModal') requestcallbackModalcontent: TemplateRef<any>;
requestcallbackModalRef: BsModalRef;
@ViewChild('casestudiescallbackModal') casestudiescallbackModalcontent: TemplateRef<any>;
casestudiescallbackModalRef: BsModalRef;
title = 'evapoler';
isCollapsed:boolean=true;
requestcallbackForm: UntypedFormGroup;
applications_evapolar_count=1;



caseStudiesOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
	margin: 20,
    navSpeed: 700,
    navText:[ '','' ],


    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false,
	lazyLoad: true
  }
  Submit_enquiry_now="Submit";
constructor(private fb: UntypedFormBuilder,private modalService: BsModalService,@Inject(DOCUMENT) private doc: Document,private connService:ConnectionService,private router: Router,private loc: Location){
this.requestcallbackForm = this.fb.group(
{
name: ['', [Validators.required]],
mobile_number: ['', [Validators.required, MobileValidations(), Validators.minLength(10), Validators.maxLength(10)]],
emailId: ['', [Validators.required, Validators.pattern("^(?!test).*$"), Validators.pattern("^(?![a-z0-9._%+-]+@[a-zA-Z0-9._%+-]*test[a-zA-Z0-9._%+-]*).*$"), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+[.][a-z]{2,4}$")]],
company_name: ['', [Validators.required]],
application:['', [Validators.required]],
city:['', [Validators.required]],
message: ['']
}
);
}



submitBookingFun(type_of_form:any){
	let desktop_mobile="";
	let page_name="";
	if (document.body.offsetWidth < 768) {
		desktop_mobile="mobile";
	}
	else{
		desktop_mobile="desktop";
	}
	if(this.loc.path()=="/industrial-cooler"){
	page_name="industrialCooler";
	}else{
	page_name="homePage";
	}
    let bodyParams={
		"name":this.requestcallbackForm?.get("name")?.value,
		"mobile_number":this.requestcallbackForm?.get("mobile_number")?.value,
		"emailId":this.requestcallbackForm?.get("emailId")?.value,
		"company_name":this.requestcallbackForm?.get("company_name")?.value,
		"application":this.requestcallbackForm?.get("application")?.value,
		"city":this.requestcallbackForm?.get("city")?.value,
		"message":this.requestcallbackForm?.get("message")?.value,
		"type_of_form":type_of_form,
		"desktop_mobile":desktop_mobile,
		"page_name":page_name
	}
	this.Submit_enquiry_now="Processing  <i class='fa fa-spin fa-refresh'></i>";
	this.connService.dumpConsultation(bodyParams).subscribe(
		(data:any) => {
		    this.Submit_enquiry_now="Submit";
			//alert(JSON.stringify(data))
			if(data["status"]=="success" || data["status"]=="exists"){
				this.requestcallbackModalRef.hide();
				this.router.navigate(['/thank-you']);
			}
		},
		error => {
			this.Submit_enquiry_now="Submit";
			alert("Oops! some issue");
			return false;
		},
		()=>{
			this.Submit_enquiry_now="Submit";
		}
	)
  }
  
  
 flag="";
 
requestCallbackFun(flag:any){
//this.doc.getElementById("enquiry_now_hidden")?.value="yes";
const input = document.getElementById(
  'enquiry_now_hidden',
) as HTMLInputElement | null;

if (input != null) {
  input.value="yes";
}


this.flag=flag;
this.requestcallbackForm.reset();
this.requestcallbackModalRef = this.modalService.show(this.requestcallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
}
requestcallbackModalRefHideFun(){
//this.doc.getElementById("enquiry_now_hidden")?.value="";
this.requestcallbackModalRef.hide();
const input = document.getElementById(
  'enquiry_now_hidden',
) as HTMLInputElement | null;

if (input != null) {
  input.value="";
}
}
caseStudiesCallbackFun(){
this.casestudiescallbackModalRef = this.modalService.show(this.casestudiescallbackModalcontent, { class: 'modal-lg', ignoreBackdropClick: true });
}
casestudiesModalRefHideFun(){
	this.casestudiescallbackModalRef.hide();
	}
}