import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
	httpOptionNoIntercept = {
		headers: new HttpHeaders({
			"Content-Type": "application/json"
		})
	};
	constructor(
		private http: HttpClient
	) {
	
	}
	dumpConsultation(input:any) {
		return this.http.post(
			"https://ics.evapoler.com/backend/action/evapoler_action.php",
			input
		);
		/*return this.http.post(
			"http://localhost/evapolar/backend/action/evapoler_action.php",
			input
		);*/
		
	}
}
