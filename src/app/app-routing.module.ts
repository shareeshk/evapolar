import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./landing-pages/home-page-version3/home-page-version3.module').then(m => m.HomePageVersion3Module)
  },
  {
    path: 'thank-you',
    loadChildren: () => import('./landing-pages/thank-you/thank-you.module').then(m => m.thankYouModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./landing-pages/privacy-policy/privacy-policy.module').then(m => m.privacyPolicyModule)
  },
  {
    path: 'industrial-cooler',
    loadChildren: () => import('./landing-pages/home-page-version4/home-page-version4.module').then(m => m.HomePageVersion4Module)
  },
  {
    path: 'original',
    loadChildren: () => import('./landing-pages/home-page-version-original/home-page-version-original.module').then(m => m.HomePageVersionOriginalModule)
  },
  {
    path: 'industrial-coolers-telangana-karnataka',
    loadChildren: () => import('./landing-pages/home-page-version5/home-page-version5.module').then(m => m.HomePageVersion5Module)
  }
];

@NgModule({
  //imports: [RouterModule.forRoot(routes, { useHash:true, scrollPositionRestoration: 'enabled'})],
  imports: [RouterModule.forRoot(routes, { useHash: false, scrollPositionRestoration: 'enabled', initialNavigation: 'enabledBlocking', anchorScrolling: 'enabled',onSameUrlNavigation: 'reload',scrollOffset: [0, 80]  })],
  exports: [RouterModule] //,
  //providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class AppRoutingModule { }
